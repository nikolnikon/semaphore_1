#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <stdio.h>


struct semid_ds semi; 

union semun {
    int              val;    /* Value for SETVAL */
    semid_ds *buf;    /* Buffer for IPC_STAT, IPC_SET */
    unsigned short  *array;  /* Array for GETALL, SETALL */
};


int main(int argc, char **argv)
{
    key_t key = ftok("/tmp/sem.temp", 1);

    int sem_id = semget(key, 16, IPC_CREAT);
    if (sem_id == -1) {
        perror("semget");
        return 1;
    }

    semun arg;

    arg.buf = &semi;
    arg.buf->sem_perm.mode = 0777;

    semctl(sem_id, 0, IPC_SET, arg.buf);
    
    for (int i = 0; i < 16; ++i) {
        arg.val = i;
        semctl(sem_id, i, SETVAL, arg.val);
    }
    
    while (1)
        ;
    return 0;
}


